#!/bin/bash

cd ./linux/

echo "make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- multi_v7_defconfig ti_multi_v7_prune.config"
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- multi_v7_defconfig ti_multi_v7_prune.config

echo "make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- savedefconfig"
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- savedefconfig

cp -v defconfig compare_defconfig

echo "Config tweaks"
./scripts/config --disable CONFIG_LOCALVERSION_AUTO
./scripts/config --enable CONFIG_DEBUG_INFO_NONE
./scripts/config --disable DEBUG_INFO_DWARF_TOOLCHAIN_DEFAULT

./scripts/config --enable CONFIG_OF_OVERLAY
./scripts/config --enable CONFIG_MODULE_COMPRESS_XZ
./scripts/config --enable CONFIG_GPIO_AGGREGATOR

#enable iwd
./scripts/config --enable CONFIG_CRYPTO_USER_API_HASH
./scripts/config --enable CONFIG_CRYPTO_USER_API_SKCIPHER
./scripts/config --enable CONFIG_KEY_DH_OPERATIONS
./scripts/config --enable CONFIG_CRYPTO_ECB
./scripts/config --enable CONFIG_CRYPTO_MD4
./scripts/config --enable CONFIG_CRYPTO_MD5
./scripts/config --enable CONFIG_CRYPTO_CBC
./scripts/config --enable CONFIG_CRYPTO_SHA256
./scripts/config --enable CONFIG_CRYPTO_AES
./scripts/config --enable CONFIG_CRYPTO_DES
./scripts/config --enable CONFIG_CRYPTO_CMAC
./scripts/config --enable CONFIG_CRYPTO_HMAC
./scripts/config --enable CONFIG_CRYPTO_SHA51

echo "make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- olddefconfig"
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- olddefconfig

cp -v .config ../public/defconfig_pre_savedefconfig

echo "make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- savedefconfig"
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- savedefconfig

cp -v defconfig ../public/defconfig

diff -u compare_defconfig defconfig

rm -rf compare_defconfig defconfig || true

cd ../
